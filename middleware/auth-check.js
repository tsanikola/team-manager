const webToken = require('jsonwebtoken');

module.exports = (req, res, next) => {
    //gets the token from the headers
    const authHeader = req.get('Authorization');
    if (!authHeader) {
        const error = new Error('Authentication failed');
        error.statusCode = 401;
        throw error;
    }
    const token = authHeader.split(' ')[1];
    let decodedToken;
    try {
        //verifies that the token is valid
        decodedToken = webToken.verify(token, process.env.JWT_SECRET);
    } catch (err) {
        err.statusCode = 500;
        throw err;
    }
    if (!decodedToken) {
        const error = new Error('Authentication failed');
        error.statusCode = 401;
        throw error;
    }
    //gets the manager's id from the token
    req.managerId = decodedToken.managerId;
    next();
};