const Member = require('../models/member');
const Team = require('../models/team');

const {
    validationResult
} = require('express-validator/check');

//runs on /manage/member/all request
exports.getMembers = (req, res, next) => {
    //get current page from params, if there is none return the first page
    const currentPage = parseInt(req.query.page) || 1;
    //get number of members shown per page from params
    //if there is none return 6 members per page
    const perPage = parseInt(req.query.perpage) || 6;
    let foundTeam;
    let totalItems;
    let totalpages;
    //get the logged in manager's team
    Team.findOne({
            managerId: req.managerId
        })
        .then(team => {
            //if no team was found return error
            if (!team) {
                const error = new Error('No team found to fetch the members.');
                error.statusCode = 401;
                throw error;
            }
            //if team was found search and count the members of that team
            foundTeam = team;
            return Member.findAndCountAll({
                where: {
                    teamId: team.id
                }
            })
        })
        .then(result => {
            totalItems = result.count;
            //if no members found, return error
            if (totalItems == 0) {
                const error = new Error('No members found');
                error.statusCode = 404;
                throw error;
            }
            //calculates which elements to show based on the give pagination values and returns them
            totalpages = totalItems % perPage > 0 ? parseInt(totalItems / perPage) + 1 : totalItems / perPage;
            return Member.findAll({
                offset: (currentPage - 1) * perPage,
                limit: perPage,
                where: {
                    teamId: foundTeam.id
                }
            })
        })
        .then(members => {
            res.status(200).json({
                message: 'Fetched members successfully.',
                members: members,
                totalItems: totalItems,
                currentPage: currentPage,
                totalPages: totalpages
            });
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
};

//runs on /manage/member/create request
exports.createMember = (req, res, next) => {
    const errors = validationResult(req);
    //returns the validation error if there was any
    if (!errors.isEmpty()) {
        const error = new Error('Ivalid data entered');
        error.statusCode = 422;
        error.data = errors.array();
        throw error;
    }
    //gets the data from the request and its body
    const managerId = req.managerId;
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const age = req.body.age;
    const position = req.body.position;
    const otherInfo = req.body.otherInfo;
    //get the logged in manager's team
    Team.findOne({
            where: {
                managerId: managerId
            }
        })
        .then(team => {
            if (!team) {
                const error = new Error('No team found.');
                error.statusCode = 404;
                throw error;
            }
            //creates a member with the given info for the found team
            return Member.create({
                firstName: firstName,
                lastName: lastName,
                age: age,
                position: position,
                otherInfo: otherInfo,
                teamId: team.id
            });
        })
        // returns the response with the new member's data
        .then(result => {
            res.status(201).json({
                message: 'Member successfully created!',
                member: result
            });
        })
        .catch(err => {
            if (err.statusCode) {
                err.statusCode = 500
            }
            next(err);
        });
};

//runs on /manage/member/get/{id} request where {id} is the member's id
exports.getMember = (req, res, next) => {
    //get the member's id from the request's params and manager's id from the request
    const memberId = req.params.memberId;
    const managerId = req.managerId;
    //get the logged in manager's team
    Team.findOne({
            where: {
                managerId: managerId
            }
        })
        .then(team => {
            if (!team) {
                const error = new Error('No team found.');
                error.statusCode = 404;
                throw error;
            }
            //search for that member in the database that belongs to that team, and therefore to that manager
            return Member.findOne({
                where: {
                    id: memberId,
                    teamId: team.id
                }
            });
        })
        .then(member => {
            //if no member was found with the given id, returns error
            if (!member) {
                const error = new Error('Member not found.');
                error.statusCode = 404;
                throw error;
            }
            //returns a response with the found member's data
            res.status(200).json({
                message: 'Member data successfully retrieved',
                member: member
            })
        }).catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500
            }
            next(err);
        });
}

//runs on /manage/member/edit/{id} request where {id} is the member's id
exports.editMember = (req, res, next) => {
    const errors = validationResult(req);
    //returns the validation error if there was any
    if (!errors.isEmpty()) {
        const error = new Error('Ivalid data entered');
        error.statusCode = 422;
        error.data = errors.array();
        throw error;
    }
    //get the member's id from the request's params and manager's id from the request
    const memberId = req.params.memberId;
    const managerId = req.managerId;
    //get the logged in manager's team
    Team.findOne({
            where: {
                managerId: managerId
            }
        })
        .then(team => {
            if (!team) {
                const error = new Error('No team found.');
                error.statusCode = 404;
                throw error;
            }
            //search for that member in the database that belongs to that team, and therefore to that manager
            return Member.findOne({
                where: {
                    id: memberId,
                    teamId: team.id
                }
            });
        })
        .then(member => {
            //if no member was found with the given id, returns error
            if (!member) {
                const error = new Error('Member not found.');
                error.statusCode = 404;
                throw error;
            }
            //gets the data from the request's body
            //if something was not set, use the existin data from the database (since we don't want to alter that data)
            member.firstName = req.body.firstName == null ? member.firstName : req.body.firstName;
            member.lastName = req.body.lastName == null ? member.lastName : req.body.lastName;
            member.age = req.body.age == null ? member.age : req.body.age;
            member.position = req.body.position == null ? member.position : req.body.position;
            member.otherInfo = req.body.otherInfo == null ? member.otherInfo : req.body.otherInfo;
            //update the found member's data
            return member.save();
        })
        .then(updated => {
            //returns a response with the found member's new data
            res.status(200).json({
                message: 'Member data successfully updated',
                member: updated
            })
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500
            }
            next(err);
        });

}

//runs on /manage/member/delete/{id} request where {id} is the member's id
exports.deleteMember = (req, res, next) => {
    //get the member's id from the request's params and manager's id from the request
    const memberId = req.params.memberId;
    const managerId = req.managerId;
    //get the logged in manager's team
    Team.findOne({
            where: {
                managerId: managerId
            }
        })
        .then(team => {
            if (!team) {
                const error = new Error('No team found.');
                error.statusCode = 404;
                throw error;
            }
            //search for that member in the database that belongs to that team, and therefore to that manager
            return Member.findOne({
                where: {
                    id: memberId,
                    teamId: team.id
                }
            });
        })
        .then(member => {
            //if no member was found with the given id, returns error
            if (!member) {
                const error = new Error('Member not found.');
                error.statusCode = 404;
                throw error;
            }
            //remove that member from the database and then return the response
            return Member.destroy({
                where: {
                    id: memberId
                }
            });
        }).then(deleted => {
            res.status(200).json({
                message: 'Member data successfully deleted'
            })
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500
            }
            next(err);
        });
}