const Manager = require('../models/manager');

const bcrypt = require('bcryptjs');
const webToken = require('jsonwebtoken');
const {
    validationResult
} = require('express-validator/check');

//it runs when a manager tries to signup
exports.signup = (req, res, next) => {
    //checks for any errors on express-validator
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const error = new Error('Ivalid data entered');
        error.statusCode = 422;
        error.data = errors.array({
            onlyFirstError: true
        });
        for (i = 0; i < error.data.length; i++) {
            if (Array.isArray(error.data[i].nestedErrors)) {
                error.data[i] = error.data[i].nestedErrors[error.data[i].nestedErrors.length - 1]
            }
        }
        throw error;
    }
    //gets the info from the body the user sent
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const password = req.body.password;
    //hashes the password
    bcrypt.hash(password, 12)
        .then(passwordHash => {
            //creates the user using the hashed password
            return Manager.create({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: passwordHash,
            })
        })
        .then(newManager => {
            //creates a new authentication token for the user by using the email and the id
            const token = webToken.sign({
                    email: newManager.email,
                    managerId: newManager.id.toString()
                },
                process.env.JWT_SECRET, {
                    expiresIn: '1h'
                }
            );
            let responseManager = {
                ...newManager.toJSON()
            }
            delete responseManager.password
            //returns a response with the registered token that the user can use to authenticate requests
            res.status(201).json({
                message: 'Manager account was created successfully!',
                token: token,
                manager: responseManager
            });
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500
            }
            next(err);
        });
};

//this runs when the user tries to edit his/her data
exports.edit = (req, res, next) => {
    //checks for any errors on express-validator
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const error = new Error('Ivalid data entered');
        error.statusCode = 422;
        error.data = errors.array({
            onlyFirstError: true
        });
        throw error;
    }
    //gets password from the body and the user id from the request
    const password = req.body.password;
    const managerId = req.managerId;
    let foundManager;
    //gets the user from the DB and checks if the given password is correct
    Manager.findOne({
            where: {
                id: managerId
            }
        })
        .then(manager => {
            if (!manager) {
                const error = new Error('Manager not found. Try to login again.');
                error.statusCode = 401;
                throw error;
            }
            foundManager = manager;
            return bcrypt.compare(password, manager.password);
        })
        .then(match => {
            if (!match) {
                const error = new Error('Wrong password');
                error.statusCode = 401;
                throw error;
            }

            //if the passwords match, fets the data from the body. 
            //If there is no new password then uses the old one, it hashes it and updates the DB with the new data
            foundManager.password = req.body.newPassword == null ? req.body.password : req.body.newPassword;
            foundManager.firstName = req.body.firstName == null ? foundManager.firstName : req.body.firstName;
            foundManager.lastName = req.body.lastName == null ? foundManager.lastName : req.body.lastName;
            foundManager.email = req.body.newEmail == null ? foundManager.email : req.body.newEmail;
            return bcrypt.hash(foundManager.password, 12)
        })
        .then(passwordHash => {
            foundManager.password = passwordHash;
            return foundManager.save();
        })
        .then(updated => {
            res.status(200).json({
                message: 'Manager data successfully updated',
                manager: updated
            })
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500
            }
            next(err);
        });
};

//this runs when the user tries to login
exports.login = (req, res, next) => {
    //gets credentials from body
    const email = req.body.email;
    const password = req.body.password;
    let foundManager;
    //check if the user exists and if the password is correct
    Manager.findOne({
            where: {
                email: email
            }
        })
        .then(manager => {
            if (!manager) {
                const error = new Error('Manager account with this email was not found.');
                error.statusCode = 401;
                throw error;
            }
            foundManager = manager;
            return bcrypt.compare(password, manager.password);
        })
        .then(match => {
            if (!match) {
                const error = new Error('Wrong password');
                error.statusCode = 401;
                throw error;
            }
            //create the token that authenticates the manager
            const token = webToken.sign({
                    email: foundManager.email,
                    managerId: foundManager.id.toString()
                },
                '34aie9$69EBpCoe06dE2eceIgi8$eJDVpTFXJ4/Y8TRF0HAPbfozx/ai9ce', {
                    expiresIn: '1h'
                }
            );
            res.status(200).json({
                message: 'You are now logged in!',
                token: token,
                manager: foundManager
            })
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500
            }
            next(err);
        });
};

//this runs when the user tries to delete their account
exports.delete = (req, res, next) => {
    //gets id from request and password from the body
    const managerId = req.managerId;
    const password = req.body.password;
    let foundManager;
    Manager.findOne({
            where: {
                id: managerId
            }
        })
        .then(manager => {
            if (!manager) {
                const error = new Error('No manager found.');
                error.statusCode = 404;
                throw error;
            }
            foundManager = manager;
            //it checks if the password is correct
            return bcrypt.compare(password, manager.password);
        })
        .then(match => {
            if (!match) {
                const error = new Error('Wrong password');
                error.statusCode = 401;
                throw error;
            }
            //if the password is correct the user is deleted
            return foundManager.destroy({
                where: {
                    managerId: managerId
                }
            });
        }).then(deleted => {
            res.status(200).json({
                message: 'Manager was successfully deleted'
            })
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500
            }
            next(err);
        });
};