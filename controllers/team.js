const Team = require('../models/team');
const {
    validationResult
} = require('express-validator/check');

//runs on /manage/team/create request
exports.createTeam = (req, res, next) => {
    //returns the validation error if there was any
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const error = new Error('Ivalid data entered');
        error.statusCode = 422;
        error.data = errors.array();
        throw error;
    }
    //gets the data from the request and its body
    const name = req.body.name;
    const managerId = req.managerId;
    //get the logged in manager's team
    Team.findOne({
            where: {
                managerId: managerId
            }
        })
        .then(team => {
            if (!team) {
                // Create the Team in db
                return Team.create({
                    name: name,
                    managerId: managerId
                });
            } else {
                //return error if the manager has already a team 
                const error = new Error('There is already a team assigned to this manager.');
                error.statusCode = 422;
                throw error;
            }
        })
        .then(result => {
            //return the response with the new team's data
            res.status(201).json({
                message: 'Team successfully created!',
                team: result
            });
        })
        .catch(err => {
            if (err.statusCode) {
                err.statusCode = 500
            }
            next(err);
        });
};

//runs on /manage/team/get request
exports.getTeam = (req, res, next) => {
    //get the manager id from request
    const managerId = req.managerId;
    //get the data of the manager's team
    this.getTeamData(managerId)
        .then(team => {
            //return the response of the data
            res.status(200).json({
                message: 'Team data successfully retrieved',
                team: team
            })
        })
        .catch(err => {
            if (err.statusCode) {
                err.statusCode = 500
            }
            next(err);
        });
}

//runs on /manage/team/edit request
exports.editTeam = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const error = new Error('Ivalid data entered');
        error.statusCode = 422;
        error.data = errors.array();
        throw error;
    }
    //get the manager id from request
    const managerId = req.managerId;
    //get the data of the manager's team
    this.getTeamData(managerId)
        .then(team => {
            //if value is null, use the old value from DB
            team.name = req.body.name == null ? team.name : req.body.name;
            //update the team data
            return team.save();
        })
        .then(updated => {
            res.status(200).json({
                message: 'Team data successfully updated',
                team: updated
            })
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500
            }
            next(err);
        });
};

//runs on /manage/team/delete request
exports.deleteTeam = (req, res, next) => {
    //get the manager id from request
    const managerId = req.managerId;
    //get the data of the manager's team, to check its existance in DB
    this.getTeamData(managerId)
        .then(team => {
            //deletes the team from DB
            return team.destroy();
        })
        .then(deleted => {
            res.status(200).json({
                message: 'Team data successfully deleted'
            })
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500
            }
            next(err);
        });
};

//gets the team info based on the manager id
exports.getTeamData = (managerId) => {
    return new Promise(
        (resolve, reject) => {
            Team.findOne({
                    where: {
                        managerId: managerId
                    }
                })
                .then(team => {
                    if (!team) {
                        const error = new Error('No team found.');
                        error.statusCode = 404;
                        reject(error);
                    }
                    resolve(team);
                })
                .catch(err => {
                    if (!err.statusCode) {
                        err.statusCode = 500
                    }
                    reject(err);
                });
        });
};