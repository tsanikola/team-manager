const express = require('express');
const {
    body,
    oneOf
} = require('express-validator/check');

const Manager = require('../models/manager');
const managerController = require('../controllers/manager');

const router = express.Router();

// POST /auth/signup
//validates the parameters that are passed from the signup request
//if the email exists in DB, returns error
router.post('/signup', [
    body('email')
    .exists().withMessage('Email field is not defined')
    .not().isEmpty()
    .isEmail()
    .withMessage('Please enter a valid email')
    .normalizeEmail()
    .custom((value, {
        req
    }) => {
        if (value) {
            return Manager.findOne({
                where: {
                    email: value
                }
            }).then(result => {
                if (result) {
                    return Promise.reject('This email already exists');
                }
            })
        }
    }),
    body('password').exists().withMessage('Password field is not defined')
    .custom((value, {
        req
    }) => {
        if (typeof value != "string") {
            throw new Error('Password must be a string of numbers, letters and/or symbols');
        }
        return true;
    })
    .not().isEmpty().withMessage('Password is required and must contain only numbers,letters and symbols')
    .trim().isLength({
        min: 5
    })
    .withMessage('Password must be at least 5 characters long'),
    oneOf([
        body('firstName').not().exists(),
        body('firstName').exists({
            checkFalsy: true,
            checkNull: true
        })
        .isAlpha().withMessage('Please enter a valid first name')
        .isLength({
            min: 3
        })
        .withMessage('First name must be at least 3 characters long')
    ]),
    oneOf([
        body('lastName').not().exists(),
        body('lastName').exists({
            checkFalsy: true,
            checkNull: true
        })
        .isAlpha().withMessage('Please enter a valid last name')
        .isLength({
            min: 3
        })
        .withMessage('Last name must be at least 3 characters long')
    ])
], managerController.signup);

// POST /auth/login
//validates the parameters that are passed from the login request
router.post('/login', [
    [
        body('email').isEmail().withMessage('Please enter a valid email').normalizeEmail(),
        body('password').trim().isLength({
            min: 5
        }).withMessage('Password is at least 5 characters long')
    ]
], managerController.login);

module.exports = router;