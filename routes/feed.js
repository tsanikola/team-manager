const express = require('express');

const memberController = require('../controllers/member');
const teamController = require('../controllers/team');
const managerController = require('../controllers/manager');

const authCheck = require('../middleware/auth-check');
const {
    body,
    oneOf
} = require('express-validator/check');

const router = express.Router();

//MANAGER ROUTES

// PUT /manage/edit
//checks for authorized access via the authCheck middleware and then
//validates the parameters that are passed from the edit request
//only the password is required.
router.put('/edit', authCheck, [
    oneOf([
        body('newEmail')
        .exists().withMessage('Email field is not defined')
        .isEmail()
        .withMessage('Please enter a valid email')
        .custom((value, {
            req
        }) => {
            return Manager.findOne({
                where: {
                    email: value
                }
            }).then(result => {
                if (result) {
                    return Promise.reject('This email is already in use');
                }
            })
        })
        .normalizeEmail(),
        body('newEmail').not().exists()
    ]),
    body('password').trim().isLength({
        min: 5
    })
    .withMessage('Password must be at least 5 characters long'),
    oneOf([
        body('newPassword').trim().isLength({
            min: 5
        })
        .withMessage('The new password must be at least 5 characters long'),
        body('newPassword').not().exists(),
    ]),
    oneOf([
        body('firstName').not().exists(),
        body('firstName').isAlpha().isLength({
            min: 3
        })
        .withMessage('First name must be at least 3 characters long'),
    ]),
    oneOf([
        body('lastName').not().exists(),
        body('lastName').isAlpha().isLength({
            min: 3
        })
        .withMessage('Lasst name must be at least 3 characters long'),
    ])
], managerController.edit);

//DELETE manage/delete
//checks for authorized access via the authCheck middleware and then
//validates the passwors passed from the delete request
router.delete('/delete', authCheck, [
        body('password').trim().isLength({
            min: 5
        })
        .withMessage('Password must be at least 5 characters long')
    ],
    managerController.delete);


//TEAM ROUTES

// GET /manage/team/get
//checks for authorized access via the authCheck middleware
router.get('/team/get', authCheck, teamController.getTeam);

// POST /manage/team/create
//checks for authorized access via the authCheck middleware and then
//validates the name passed from the request
router.post('/team/create', authCheck, [
        body('name').isLength({
            min: 2
        }).isAlphanumeric()
        .withMessage('Team name must be at least 2 characters long')
    ],
    teamController.createTeam);

// PUT /manage/team/edit 
//checks for authorized access via the authCheck middleware and then
//validates the name passed from the edit request
router.put('/team/edit', authCheck, [
        body('name').isLength({
            min: 2
        }).isAlphanumeric()
        .withMessage('Team name must be at least 2 characters long')
    ],
    teamController.editTeam);

//DELETE /manage/team/delete
//checks for authorized access via the authCheck middleware 
router.delete('/team/delete', authCheck, teamController.deleteTeam);


//MEMBER ROUTES

// GET /manage/member/all?page=2&perpage=6
//checks for authorized access via the authCheck middleware 
//can have parameters for pagination manipulation
router.get('/member/all', authCheck, memberController.getMembers);

// GET /manage/member/get/4
//checks for authorized access via the authCheck middleware 
router.get('/member/get/:memberId', authCheck, memberController.getMember);

// POST /manage/member/create
//checks for authorized access via the authCheck middleware and then
//validates the parameters that are passed from the request
//first name and last name required.
router.post('/member/create', authCheck, [
        body('firstName').isLength({
            min: 3
        }).isAlpha()
        .withMessage('First name must be at least 3 characters long'),
        body('lastName').isLength({
            min: 3
        }).isAlpha()
        .withMessage('Last name must be at least 3 characters long'),
        oneOf([
            body('age').not().exists(),
            body('age').isInt(),
        ]),
        oneOf([
            body('position').not().exists(),
            body('position').isAlphanumeric(),
        ]),
        oneOf([
            body('otherInfo').not().exists(),
            body('otherInfo')
        ])
    ],
    memberController.createMember);

// PUT /manage/member/edit/5 
//checks for authorized access via the authCheck middleware and then
//validates the parameters that are passed from the edit request
//all the parameters are optional
router.put('/member/edit/:memberId', authCheck, [
        oneOf([
            body('firstName').not().exists(),
            body('firstName').isLength({
                min: 3
            }).isAlpha()
            .withMessage('First name must be at least 3 characters long'),
        ]),
        oneOf([
            body('lastName').not().exists(),
            body('lastName').isLength({
                min: 3
            }).isAlpha()
            .withMessage('Last name must be at least 3 characters long'),
        ]),
        oneOf([
            body('age').not().exists(),
            body('age').isInt(),
        ]),
        oneOf([
            body('position').not().exists(),
            body('position').isAlphanumeric(),
        ]),
        oneOf([
            body('otherInfo').not().exists(),
            body('otherInfo')
        ])
    ],
    memberController.editMember);

//DELETE /manage/member/delete/2
//checks for authorized access via the authCheck middleware 
router.delete('/member/delete/:memberId', authCheck, memberController.deleteMember);

module.exports = router;