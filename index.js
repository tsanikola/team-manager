const sequelize = require('./util/database');
const app = require('./app');


sequelize.sync()
    .then(res => {
        app.listen(process.env.APP_PORT);
        console.log("running on " + process.env.APP_PORT);
    })
    .catch(err => {
        console.log(err);
    });