const Sequelize = require('sequelize');
const sequelize = require('../util/database');

//the member's sequelize model
const Member = sequelize.define('member', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    firstName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    lastName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    age: Sequelize.INTEGER,
    position: Sequelize.STRING,
    otherInfo: Sequelize.STRING
});

module.exports = Member;