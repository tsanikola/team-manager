const Sequelize = require('sequelize');
const sequelize = require('../util/database');

//the team's sequelize model
const Team = sequelize.define('team', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
});

module.exports = Team;