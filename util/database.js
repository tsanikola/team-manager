const Sequelize = require('sequelize');
//connect to the database
//-->sequelize('database_name', 'username', 'password')
// const sequelize = new Sequelize('team_manager', 'root', 'root', {
const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
    dialect: process.env.DB_DIALECT,
    host: process.env.DB_HOST,
    logging: false //--> true this for DB logging
});

module.exports = sequelize;